
Course
    Curso completo de Desenvolvimento iOS11/Swift4: Crie 20 Apps
    
Tutor
    Eric Alves Brito

Module Description
    Create an app that uses the device camera to identify objects with CoreML.
    
    
Screenshots
    
    

![LaunchScreen](/Screens/screen01.png)  ![WelcomeScreen](/Screens/screen02.png)   ![DetectScreen](/Screens/screen03.png)
